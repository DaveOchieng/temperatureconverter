package com.example.davidochieng.temperatureconverter;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {
    private EditText editDegree, editFahrenheit;
    private TextView editDegreeLabel, viewFahrenheitLabel, viewFahrenheit, editFahrenheitLabel,
            viewDegreeLabel, viewDegree ;
    private Button d2fButton, f2dButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        editDegree=(EditText) findViewById(R.id.editDegree);
        editFahrenheit=(EditText) findViewById(R.id.editFahrenheit);

        editDegreeLabel=(TextView) findViewById(R.id.editDegreeLabel);
        viewFahrenheitLabel=(TextView) findViewById(R.id.viewFahrenheitLabel);
        viewFahrenheit=(TextView) findViewById(R.id.viewFahrenheit);
        viewDegreeLabel=(TextView) findViewById(R.id.viewDegreeLabel);
        viewDegree=(TextView) findViewById(R.id.viewDegree);

        d2fButton=(Button) findViewById(R.id.d2fButton);
        f2dButton=(Button) findViewById(R.id.f2dButton);

        d2fButton.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                if(!editDegree.getText().toString().isEmpty()){
                    double tempInDegrees=Double.parseDouble(String.valueOf(editDegree.getText()));
                    double tempInFahrenheit= ((tempInDegrees * 9) / 5) + 32;
                    viewFahrenheit.setText(Double.toString(tempInFahrenheit));

                }

            }
        });

        f2dButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!editFahrenheit.getText().toString().isEmpty()){
                    double tempInFahrenheit=Double.parseDouble(String.valueOf(editFahrenheit.getText()));
                    double tempInDegrees=(tempInFahrenheit-32)*5/9;

                    viewDegree.setText(Double.toString(tempInDegrees));
                }
            }
        });
    }

}
